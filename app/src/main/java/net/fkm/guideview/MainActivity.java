package net.fkm.guideview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private DisplayMetrics dm;
    private LinearLayout layout, layoutLeft, layoutCenter, layoutRight;
    private static Handler messageHandler;
    private Intent intent;
    @SuppressLint("WrongConstant")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        // 启动服务播放背景音乐
        intent = new Intent(MainActivity.this, MyIntentService.class);
        String action = MyIntentService.ACTION_MUSIC;
        // 设置action
        intent.setAction(action);
        startService(intent);

        super.onCreate(savedInstanceState);

        dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        layout = new LinearLayout(this);
        layout.setLayoutParams(new LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        layout.setOrientation(0); //水平
        layout.setGravity( Gravity.CENTER);

        layoutLeft = new LinearLayout(this);
        layoutLeft.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT));
        layoutLeft.setGravity( Gravity.RIGHT);

        layoutCenter = new LinearLayout(this);
        layoutCenter.setLayoutParams(new LayoutParams(dm.widthPixels, LayoutParams.WRAP_CONTENT));

        layoutRight = new LinearLayout(this);
        layoutRight.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT));
        layoutRight.setGravity( Gravity.LEFT);

        final TextView tview =new TextView( this );
        tview.setText( "任凭弱水三千，我只取一瓢饮！" );
        //给TextView控件设置文字颜色
        tview.setTextColor( Color.RED);
        //给TextView控件设置文字大小
        tview.setTextSize(25);
        tview.setVisibility( View.INVISIBLE);

        ImageView heart = new ImageView(this);
        heart.setLayoutParams(new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));  //image的布局方式
        heart.setImageResource(R.drawable.heart);  //设置imageview呈现的图片

        //设置字体样式测试失败
//        AssetManager mgr=getAssets();//得到AssetManager
//        Typeface tf= Typeface.createFromAsset(mgr, "fonts/fontxinwei.TTF");//根据路径得到Typeface
//        tview.setTypeface(tf);//设置字体

        ImageView viewboy = new ImageView(this);
        viewboy.setLayoutParams(new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));  //image的布局方式
        viewboy.setImageResource(R.drawable.lovelyboy);  //设置imageview呈现的图片


        ImageView viewgirl = new ImageView(this);
        viewgirl.setLayoutParams(new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));  //image的布局方式
        viewgirl.setImageResource(R.drawable.lovelygirl);  //设置imageview呈现的图片

        layoutLeft.addView( viewboy );
        layoutRight.addView( viewgirl );
        layoutCenter.addView( heart );
        layoutCenter.addView( tview );

       //设置背景图片
        Resources resources = layout.getContext().getResources();
        Drawable btnDrawable = resources.getDrawable(R.drawable.timg);
        layout.setBackgroundDrawable(btnDrawable);

        layout.addView(layoutLeft);
        layout.addView(layoutCenter);
        layout.addView(layoutRight);

        setContentView(layout);

        Looper looper = Looper.myLooper();
        messageHandler = new MessageHandler(looper);

        new Thread( new Runnable()
        {
            @Override
            public void run()
            {
                int speed = 20;		//每次移动间隔毫秒，数字越大越慢
                int speedPx = 1;	//每次移动间隔像素，数字越大越快
                int loopCount = dm.widthPixels/2;
                int i=1;
                while(i < loopCount)
                {
                    Message message = Message.obtain();
                    message.what = 1;
                    message.arg1 = i;
                    messageHandler.sendMessage(message);

                    i = i + speedPx;

                    synchronized(this)
                    {
                        try
                        {
                            wait(speed);

                        }
                        catch (InterruptedException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }

                i=0;
                while(i <1)//数值改变图片间隔距离，越大间隔越大，越小间隔越小
                {
                    Message message = Message.obtain();
                    message.what = 1;
                    message.arg1 = loopCount - i;
                    messageHandler.sendMessage(message);

                    i = i + speedPx;

                    synchronized(this)
                    {
                        try
                        {
                            wait(speed);

                        }
                        catch (InterruptedException e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();

    }

    class MessageHandler extends Handler
    {
        public MessageHandler(Looper looper)
        {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case 1:
                    layoutLeft.setLayoutParams(new LayoutParams(msg.arg1, LayoutParams.WRAP_CONTENT));
                    layoutRight.setLayoutParams(new LayoutParams(msg.arg1, LayoutParams.WRAP_CONTENT));
                    layoutCenter.setLayoutParams(new LayoutParams(dm.widthPixels - msg.arg1 * 2, LayoutParams.WRAP_CONTENT));
                    break;
//                但LayoutParams类也只是简单的描述了宽高，宽和高都可以设置成三种值：
//                1，一个确定的值；
//                2，FILL_PARENT，即填满（和父容器一样大小）；
//                3，WRAP_CONTENT，即包裹住组件就好。
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (intent != null){
            // 对于intentService，这一步可能是多余的
            stopService(intent);
        }
    }
}

