package net.fkm.guideview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class RedPacketActivity extends AppCompatActivity implements View.OnClickListener {
    private RedPacketView redRainView1;
    AlertDialog.Builder ab;
    //弹框
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.redpacket);
        ab = new AlertDialog.Builder(RedPacketActivity.this);
        redRainView1 = (RedPacketView) findViewById(R.id.red_packets_view1);
        startRedRain();
    }

    @Override
    public void onClick(View v) {
    }

    /**
     * 开始下红包雨
     */
    private void startRedRain() {
        redRainView1.startRain();
        redRainView1.setOnRedPacketClickListener(new RedPacketView.OnRedPacketClickListener() {
            @Override
            public void onRedPacketClickListener(RedPacket redPacket) {
                redRainView1.pauseRain();
                ab.setCancelable(false);
                ab.setTitle("获奖提示");
                ab.setMessage("恭喜优秀的小姐姐！\n获得超级无敌宇宙帅气\n一心一意只对你好的小哥哥一枚！");
                ab.setNegativeButton("前往领取→", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // redRainView1.restartRain();
                        Intent it = new Intent();
                        it.setClass(RedPacketActivity.this,TestMovieActivity.class);
                        RedPacketActivity.this.startActivity(it);
                    }
                });

                redRainView1.post(new Runnable() {
                    @Override
                    public void run() {
                        ab.show();
                    }
                });
            }
        });
    }

    /**
     * 停止下红包雨
     */
    private void stopRedRain() {
        redRainView1.stopRainNow();
    }
}