package net.fkm.guideview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ConfessionActivity extends AppCompatActivity {
    private TextView textView;

    String s ="\n\t\n\t\n\t\n\t\n\t\n\t\n\t\n\t\n\t"+
        "\n\t\t\t\t\t心\t心\t心\t\t\t\t\t\t\t\t\t\t\t\t\t" +
        "\n\t\t\t\t心\t\t\t\t\t\t\t心\t\t\t\t\t\t\t"+
        "\n\t\t\t心\t\t\t\t\t\t\t\t\t心\t\t\t\t\t"+
        "\n\t\t\t\t心\t\t\t\t\t\t\t心\t\t\t\t\t" +
        "\n\t\t\t\t\t心\t心\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
        "\n\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤\t❤\t\t❤\t❤" +
        "\n\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t心\t\t\t❤\t\t\t\t\t\t\t❤\t\t\t\t\t\t❤" +
        "\n\t\t\t\t\t\t\t\t\t心\t心\t心\t心\t\t\t\t\t❤\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤" +
        "\n\t\t\t\t\t\t\t\t\t心\t心\t心\t心\t\t\t\t\t\t❤\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤" +
        "\n\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t❤\t\t\t\t\t\t\t\t\t\t\t❤" +
        "\n\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤\t\t\t\t\t❤" +
        "\n\t\t\t\t\t\t\t心\t心\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤" +
        "\n\t\t\t\t\t\t心\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
        "\n\t\t\t\t\t心\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
        "\n\t\t\t\t心\t\t\t\t\t\t\t\t\t\t心\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
        "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" +
        "\n\t\t\t我\t是\t九\t，\t你\t是\t三\t，\t除\t了\t你\t还\t是\t你。\t\t\t" +
        "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

    private TiaoZiUtil tiaoziUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.confession);//加载xml文件

        //获取文本控件
        textView = ((TextView) findViewById(R.id.tv_text));
        //字体类型
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/STXINGKA.TTF");
        textView.setTypeface(typeface);
        //字体大小
        textView.setTextSize(20);
        //字体颜色
        textView.setTextColor( Color.RED);
        //文本逐个显示
        tiaoziUtil = new TiaoZiUtil(textView, s, 20);//调用构造方法，直接开启


        //TextView控件水平垂直居中
        RelativeLayout.LayoutParams layoutParams=
                new RelativeLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule( RelativeLayout.CENTER_IN_PARENT);
        textView.setLayoutParams(layoutParams);
        handler.post(waitSendsRunnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //启用一个Handler

    Handler handler = new Handler() {

        @SuppressLint("HandlerLeak")

        public void handleMessage(Message msg) {

            super.handleMessage(msg);

            switch (msg.what) {

                case 0:

                    Intent intent = new Intent(ConfessionActivity.this, RedPacketActivity.class);

                    startActivity(intent);

                    break;

                case 1:


                    break;

                default:

                    break;

            }

        }

    };

// 倒计时17秒

    int index = 16;

    Runnable waitSendsRunnable = new Runnable() {

        public void run() {

            if (index > 0) {

                index--;

                try {

                    Thread.sleep(1000);

                    handler.sendEmptyMessage(1);

                } catch (InterruptedException e) {

                    e.printStackTrace();

                }

                handler.post(waitSendsRunnable);

            } else {

                try {

                    Thread.sleep(1000);

                    handler.sendEmptyMessage(0);

                } catch (InterruptedException e) {

                    e.printStackTrace();

                }

            }

        }

    };
}
