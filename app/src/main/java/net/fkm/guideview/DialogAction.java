package net.fkm.guideview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DialogAction extends AppCompatActivity implements View.OnClickListener {

    AlertDialog.Builder ab;
    AlertDialog.Builder ab1;
    AlertDialog.Builder ab2;
    AlertDialog.Builder ab3;
    AlertDialog.Builder ab4;
    AlertDialog.Builder ab5;
    AlertDialog.Builder ab6;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog);
        ab = new AlertDialog.Builder(DialogAction.this);
        ab1 = new AlertDialog.Builder(DialogAction.this);
        ab2 = new AlertDialog.Builder(DialogAction.this);
        ab3 = new AlertDialog.Builder(DialogAction.this);
        ab4 = new AlertDialog.Builder(DialogAction.this);
        ab5 = new AlertDialog.Builder(DialogAction.this);
        ab6 = new AlertDialog.Builder(DialogAction.this);
        ab.setCancelable(false);
        ab.setTitle("小姐姐：");
        ab.setMessage("I want to see the stars with you.");
        ab.setNegativeButton("yes, I do",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ab1 = new AlertDialog.Builder(DialogAction.this);
                ab1.setCancelable(false);
                ab1.setTitle("亲爱的：");
                ab1.setMessage("我会做饭给你吃");
                ab1.setNegativeButton("然后呢",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ab2 = new AlertDialog.Builder(DialogAction.this);
                        ab2.setCancelable(false);
                        ab2.setTitle("亲爱的：");
                        ab2.setMessage("以后的房本也写你的名字");
                        ab2.setNegativeButton("快去看看我为你准备的惊喜吧→",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent it = new Intent();
                                it.setClass(DialogAction.this,MainActivity.class);
                                DialogAction.this.startActivity(it);
                            }
                        });
                        ab2.show();
                    }
                });
                ab1.show();
            }
        });
        ab.setPositiveButton("再想想",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            ab1 = new AlertDialog.Builder(DialogAction.this);
            ab1.setCancelable(false);
            ab1.setTitle("小姐姐：");
            ab1.setMessage("你忍心看我痛哭流涕吗？！\n我妈会游泳！！\n保大！！");
            ab1.setNegativeButton("忍心",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ab2 = new AlertDialog.Builder(DialogAction.this);
                    ab2.setCancelable(false);
                    ab2.setTitle("小姐姐：");
                    ab2.setMessage("我不会放弃的！！");
                    ab2.setNegativeButton("坚决拒绝",new DialogInterface.OnClickListener() {
//                      @Override
                             public void onClick(DialogInterface dialog, int which) {
                            ab3 = new AlertDialog.Builder(DialogAction.this);
                            ab3.setCancelable(false);
                            ab3.setTitle("系统提示");
                            ab3.setMessage("温馨提示，由于拒绝次数过多，10s后将自动关机重启，请谨慎选择");
                            ab3.setNegativeButton("关吧",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ab4 = new AlertDialog.Builder(DialogAction.this);
                                    ab4.setCancelable(false);
                                    ab4.setTitle("小姐姐：");
                                    ab4.setMessage("骗你的，我怎么会做这种事情呢");
                                    ab4.setNegativeButton("还是去看看我为你准备的惊喜吧→",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent it = new Intent();
                                            it.setClass(DialogAction.this,MainActivity.class);
                                            DialogAction.this.startActivity(it);
                                        }
                                    });
                                    ab4.show();
                                }
                            });ab3.setPositiveButton("等一下",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ab4 = new AlertDialog.Builder(DialogAction.this);
                                    ab4.setCancelable(false);
                                    ab4.setTitle("小姐姐：");
                                    ab4.setMessage("你犹豫了，你犹豫了对吧");
                                    ab4.setNegativeButton("那就去看看我为你准备的惊喜吧→",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent it = new Intent();
                                            it.setClass(DialogAction.this,MainActivity.class);
                                            DialogAction.this.startActivity(it);
                                        }
                                    });
                                    ab4.show();
                                }
                            });
                            ab3.show();
                        }
                    });
                    ab2.setPositiveButton("再考虑考虑吧",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ab3 = new AlertDialog.Builder(DialogAction.this);
                            ab3.setCancelable(false);
                            ab3.setTitle("小姐姐：");
                            ab3.setMessage("我就知道你还是心疼我的");
                            ab3.setNegativeButton("快去看我为你准备的惊喜→",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent it = new Intent();
                                    it.setClass(DialogAction.this,MainActivity.class);
                                    DialogAction.this.startActivity(it);
                                }
                            });
                        }
                    });
                    ab2.show();
                }
            });
            ab1.setPositiveButton("不忍心",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ab2 = new AlertDialog.Builder(DialogAction.this);
                    ab2.setCancelable(false);
                    ab2.setTitle("小姐姐：");
                    ab2.setMessage("我就知道你还是心疼我的");
                    ab2.setNegativeButton("快去看我为你准备的惊喜→",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent it = new Intent();
                            it.setClass(DialogAction.this,MainActivity.class);
                            DialogAction.this.startActivity(it);
                        }
                    });
                    ab2.show();
                }
            });
            ab1.show();
            }
        });
        ab.show();
    }

    @Override
    public void onClick(View v) {
    }
}