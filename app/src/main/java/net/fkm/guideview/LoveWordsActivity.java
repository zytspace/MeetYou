package net.fkm.guideview;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Alan on .
 */
public class LoveWordsActivity extends AppCompatActivity {
    private TextView textView;
    private DisplayMetrics dm;
    private Intent intent;
    private Button bt1;
//String s =
//      "\t\t\t\t\t\t\t\t来者何人\t\t\t\t\t\t\t你的人\t\t\t\t" +
//    "\n\t\t\t\t\t\t\t古云落叶归根\t\t\t\t那么我归你\t\t\t\t\t"+
//    "\n\t\t\t你看，我手里有一把土，你要栽在我手里吗\t\t\t"+
//    "\n\t\t大年三十晚上的鞭炮在响，也没有我想你那么想\t\t" +
//    "\n\t从今天开始我只能称呼你为“您”了，因为你在我心上\t" +
//    "\n\t最近有谣言说我喜欢你，我要澄清一下，那不是谣言\t" +
//    "\n\t\t你不觉得累吗？你已经在我的世界里跑了好几圈了\t"+
//    "\n\t\t\t\t你知道我最大的缺点是什么吗？就是缺点你\t\t" +
//    "\n\t\t\t\t\t\t我觉得你特别的像一款游戏，我的世界\t\t\t" +
//    "\n\t\t\t\t\t\t\t你的笑容没有酒，我却醉的像条狗\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t\t猪撞树上了，你撞我心里了\t\t\t\t\t" +
//    "\n\t\t\t\t\t\t\t\t\t\t\t这是手背，这是脚背\t\t\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t你是我的宝贝\t\t\t\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤\t❤\t❤\t\t\t\t\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤\t\t\t\t\t\t\t\t\t\t";

//String s =
//      "\t\t\t\t\t\t我很笨，\t\t\t\t\t\t\t我不知道\t\t\t\t\t\t" +
//    "\n\t\t\t\t\t要怎么表达我\t\t\t\t心中对你的感\t\t\t\t\t"+
//    "\n\t\t\t觉。我知道牵手容易,但是牵一辈子,是很\t\t\t\t"+
//    "\n\t\t难的。可是我就是想和你牵手一起走过每个人\t\t\t" +
//    "\n\t生的路口,我不敢说我可以给你一切,但是只要我可\t\t" +
//    "\n以给的我都愿意而且无怨无悔。喜欢你,喜欢到都恨自" +
//    "\n\t己,恨自己不能当着你的面说喜欢。我不知道你会接\t\t" +
//    "\n\t受或者拒绝,在写这封信的时候也不去想那么多了，\t\t\t" +
//    "\n\t\t\t只是想把现在心里所有的话都写出来,让你知\t\t\t\t" +
//    "\n\t\t\t\t道,在你的身边,一直有一个人,因为你的笑\t\t\t\t\t" +
//    "\n\t\t\t\t\t靥而开心,因为你的皱眉而伤神。你微笑,\t\t\t\t\t" +
//    "\n\t\t\t\t\t\t我可以开心好几天。就像向日葵\t\t\t\t\t\t" +
//    "\n\t\t\t\t\t\t\t一样,看到太阳就会露出微笑,\t\t\t\t\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t感觉到一切的美好。你是\t\t\t\t\t\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t\t\t我心中的小太阳\t\t\t\t\t\t\t\t\t\t" +
//    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t❤❤❤\t\t\t\t\t\t\t\t\t\t\t"+
//    "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t❤\t\t\t\t\t\t\t\t\t\t\t";

    String s =
            "                   我很笨，                    我不知道               " +
                    "\n           要怎么表达我                心中对你的感          " +
                    "\n   觉。我知道牵手容易， 但是牵一辈子，是很难" +
                    "\n   的。可是我就是想和你牵手一起走过每个人生" +
                    "\n 的路口，我不敢说我可以给你一切，但是只要是我" +
                    "\n 可以给的我都愿意而且无怨无悔。喜欢你，喜欢到" +
                    "\n 都恨自己，恨自己不能当着你的面说喜欢。我不知" +
                    "\n   道你会接受或者拒绝，在写这封 信 的时 候 也不  " +
                    "\n  去想那么多了， 只是想把现 在 心 里 所 有的话 " +
                    "\n     都 写 出 来，让你知道，在你 身边，一直有" +
                    "\n        一个人，因为你的笑靥而开心，因为你的" +
                    "\n             皱眉而伤神。你微笑，我可以开心好      " +
                    "\n                  几天。就像向日葵一样，看到        " +
                    "\n                     太阳就会露出微笑，感觉          " +
                    "\n                        到一切的 美 好 。 你            " +
                    "\n                           是我心中的小太阳            " +
                    "\n                                 我，真 的  ，              " +
                    "\n                                   想牵起你                " +
                    "\n                                      的手                  " +
                    "\n                                       。                 ";
    private TiaoZiUtil tiaoziUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 启动服务播放背景音乐
        intent = new Intent( LoveWordsActivity.this, wordIntentService.class );
        String action = wordIntentService.ACTION_MUSIC;
        // 设置action
        intent.setAction( action );
        startService( intent );
        dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( dm );

        super.onCreate( savedInstanceState );
        setContentView( R.layout.lovewords );//加载xml文件

        //获取文本控件
        textView = ((TextView) findViewById( R.id.tv_text ));
        //字体类型
        Typeface typeface = Typeface.createFromAsset( getAssets(), "fonts/STXINWEI.TTF" );
        //textView.setTypeface(typeface);
        //字体大小
        textView.setTextSize( 16 );
        //字体颜色
        textView.setTextColor( Color.RED );
        //文本逐个显示
        tiaoziUtil = new TiaoZiUtil( textView, s, 100 );//调用构造方法，直接开启

        bt1 =findViewById(R.id.button);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoveWordsActivity.this , DialogAction.class);
                //启动
                startActivity(i);
            }
        });
        //TextView控件水平垂直居中
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
        layoutParams.addRule( RelativeLayout.CENTER_IN_PARENT );
        textView.setLayoutParams( layoutParams );


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (intent != null){
            // 对于intentService，这一步可能是多余的
            stopService(intent);
        }
    }
}
